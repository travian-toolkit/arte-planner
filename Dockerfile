FROM "node"

RUN mkdir /app
WORKDIR /app
COPY package.json package-lock.json /app/
COPY frontend /app/frontend
RUN npm install && cd frontend && npm install
COPY . /app

RUN npm run deploy

CMD [ "npm", "run", "start" ]
